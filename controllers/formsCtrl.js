import angular from 'angular';

angular.module('formsCtrl',['$http'])
.controller('formsCtrl', ($http) => {
    let vm = this;
    vm.fields = [];
    $http.post('').success((data) =>{
        vm.fields = data;
    });
});