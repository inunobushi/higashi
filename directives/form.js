import angular from 'angular';

angular.module('forms', [])
.directive('forms',() => {
return {
    restrict: 'E',
    templateUrl:'../views/partials/forms.html',
    require:'^ngModel',
    controller:'formCtrl',
    controllerUrl:'../controllers/formsCtrl.js'
    };
});