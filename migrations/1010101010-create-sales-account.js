'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Sales', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      store: {
        type: Sequelize.STRING,
        unique: true
      },
      location: {
        type: Sequelize.STRING
      },
      pointOfContactEmail: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      pointOfContactName: {
        type: Sequelize.STRING
    }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('Sales');
  }
};