'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Merch', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      product: {
        type: Sequelize.STRING,
        unique: true
      },
      rep: {
        type: Sequelize.STRING
      },
      startTime: {
        type: Sequelize.TIME
      },
      endTime: {
        type: Sequelize.TIME
      },
      date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      storeName: {
        type: Sequelize.STRING
    },
    storeLocation: {
        type: Sequelize.STRING
    },
    locationInStore: {
        type: Sequelize.STRING
    },
    positionShelf: {
        type: Sequelize.STRING
    },
    skuNumber: {
        type: Sequelize.INTEGER
    },
    shelfCount: {
        type: Sequelize.INTEGER
    },
    shelfFront: {
        type: Sequelize.BOOLEAN
    },
    inventoryBack: {
        type: Sequelize.BOOLEAN
    },
    placedOrder: {
        type: Sequelize.BOOLEAN
    },
    pointOfContact: {
        type: Sequelize.STRING
    },
    notes: {
        type: Sequelize.STRING
    }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('Merch');
  }
};