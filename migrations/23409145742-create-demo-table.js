'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Demos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      product: {
        type: Sequelize.STRING,
        unique: true
      },
      rep: {
        type: Sequelize.STRING
      },
      startTime: {
        type: Sequelize.TIME
      },
      endTime: {
        type: Sequelize.TIME
      },
      date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      storeName: {
        type: Sequelize.STRING
    },
    storeLocation: {
        type: Sequelize.STRING
    },
    locationInStore: {
        type: Sequelize.STRING
    },
    flavors: {
        type: Sequelize.INTEGER
    },
    countIn: {
        type: Sequelize.INTEGER
    },
    countOut: { 
        type: Sequelize.INTEGER
    },
    totalSold: {
        type: Sequelize.INTEGER
    },
    reimbursementRequired: {
        type: Sequelize.INTEGER
    },
    notes: {
        type: Sequelize.STRING
    }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('Demos');
  }
};