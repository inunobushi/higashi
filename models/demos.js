'use strict'

module.exports = function(sequelize, DataTypes){
    var demoImport = sequelize.define('demoImport', {

        product:{
            type: DataTypes.STRING,
            unique:true
        },
        rep:{
            type: DataTypes.STRING
        },
        startTime: {
            type: DataTypes.TIME
        },
        endTime: {
            type: DataTypes.TIME
        },
        date: {
            allowNull: false,
            type: DataTypes.DATE
        },
        storeName: {
            type: DataTypes.STRING
        },
        storeLocation: {
            type: DataTypes.STRING
        },
        locationInStore: {
            type: DataTypes.STRING
        },
        flavors: {
            type: DataTypes.INTEGER
        },
        countIn: {
            type: DataTypes.INTEGER
        },
        countOut: { 
            type: DataTypes.INTEGER
        },
        totalSold: {
            type: DataTypes.INTEGER
        },
        reimbursementRequired: {
            type: DataTypes.INTEGER
        },
        notes: {
            type: DataTypes.STRING
        }
    });
    return demoImport;
};