"use strict";

module.exports = function(sequelize, DataTypes) {
  var Merch = sequelize.define("Merch", {      
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      product: {
        type: DataTypes.STRING,
        unique: true
      },
      rep: {
        type: DataTypes.STRING,
      },
      startTime: {
        type: DataTypes.TIME,
      },
      endTime: {
        type: DataTypes.TIME,
      },
      date: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      storeName: {
        type: DataTypes.STRING,
    },
    storeLocation: {
        type: DataTypes.STRING,
    },
    locationInStore: {
        type: DataTypes.STRING,
    },
    positionShelf: {
        type: DataTypes.STRING,
    },
    skuNumber: {
        type: DataTypes.INTEGER,
    },
    shelfCount: {
        type: DataTypes.INTEGER,
    },
    shelfFront: {
        type: DataTypes.BOOLEAN,
    },
    inventoryBack: {
        type: DataTypes.BOOLEAN,
    },
    placedOrder: {
        type: DataTypes.BOOLEAN,
    },
    pointOfContact: {
        type: DataTypes.STRING,
    },
    notes: {
        type: DataTypes.STRING,
    }
});

  return Merch;
};