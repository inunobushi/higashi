"use strict";

module.exports = function(sequelize, DataTypes) {
  var Sales = sequelize.define("Sales", {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      store: {
        type: DataTypes.STRING,
        unique: true,
      },
      location: {
        type: DataTypes.STRING,
      },
      pointOfContactEmail: {
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      pointOfContactName: {
        type: DataTypes.STRING,
    }
  });

  return Sales;
};

    