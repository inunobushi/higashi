
var angular = require('angular');

angular.module("fswd", [require('./fswd/registration').name, require('./fswd/dashboard').name, require('./fswd/sales').name, require('./fswd/demo').name, require('./fswd/merch').name, require('/fswd/export').name, require('angular-route/index')])

angular.bootstrap(document, ['fswd']);
