
var angular = require('angular');

angular.module('fswd.dashboard', [require('angular-route/index')])
  .config(function($routeProvider) {
   $routeProvider.when('/dashboard', {
    templateUrl: '/partials/dashboard',
    controller:'dashboardCtrl',
    controllerUrl:'/controllers/dashboardCtrl.js'
   });
   
  })

  module.exports = angular.module('fswd.dashboard')