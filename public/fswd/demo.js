
var angular = require('angular');

angular.module('fswd.demo', [require('angular-route/index')])
  .config(function($routeProvider) {
   $routeProvider.when('/demo', {
    templateUrl: '/partials/demo',
    controller:'demoCtrl',
    controllerUrl:'/controllers/demoCtrl.js'
   });
   
  })

  module.exports = angular.module('fswd.demo')