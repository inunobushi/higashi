
var angular = require('angular');

angular.module('fswd.export', [require('angular-route/index')])
  .config(function($routeProvider) {
   $routeProvider.when('/export', {
    templateUrl: '/partials/export',
    controller: 'exportCtrl',
    controllerUrl:'/controllers/exportCtrl.js'
   });
   
  })

  module.exports = angular.module('fswd.export')