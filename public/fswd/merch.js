
var angular = require('angular');

angular.module('fswd.merch', [require('angular-route/index')])
  .config(function($routeProvider) {
   $routeProvider.when('/merch', {
    templateUrl: '/partials/merch',
    controller: 'merchCtrl',
    controllerUrl:'/controllers/merchCtrl.js'
   });
   
  })

  module.exports = angular.module('fswd.merch')