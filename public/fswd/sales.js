
var angular = require('angular');

angular.module('fswd.sales', [require('angular-route/index')])
  .config(function($routeProvider) {
   $routeProvider.when('/sales', {
    templateUrl: '/partials/sales',
    controller:'salesCtrl',
    controllerUrl:'/controllers/salesCtrl.js'
   });
   
  })

  module.exports = angular.module('fswd.sales')